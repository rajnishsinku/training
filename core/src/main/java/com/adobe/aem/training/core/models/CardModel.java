package com.adobe.aem.training.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Named;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CardModel {

    private static final Logger logger = LoggerFactory.getLogger(CardModel.class);

    @ValueMapValue
    private String cardimage;

    @ValueMapValue
    @Default(values = "Blank Name")
    private String fullname;

    @ValueMapValue
    private String description;

    @ValueMapValue
    @Named("sling:resourceType")
    private String resourceType;

    @PostConstruct
    protected void init() {
        logger.debug("Card Image: {}", cardimage);
        logger.debug("Full Name: {}", fullname);
        logger.debug("Description: {}", description);

        logger.debug("Manipulating Full Name (Applying a logic)");
        fullname = fullname.toUpperCase();
        logger.debug("Upper Case Full Name: {}", fullname);
        logger.debug("Sling Resource Type: {}", resourceType);
    }

    public String getCardimage() {
        return cardimage;
    }

    public String getFullname() {
        return fullname;
    }

    public String getDescription() {
        return description;
    }
}
